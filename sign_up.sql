-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2019 at 06:45 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sign_up`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE `assignment` (
  `coursecode` varchar(50) NOT NULL,
  `assignmentDetails` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignmentdetails`
--

CREATE TABLE `assignmentdetails` (
  `coursecode` varchar(50) NOT NULL,
  `details` text NOT NULL,
  `intake` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `facultyname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignmentdetails`
--

INSERT INTO `assignmentdetails` (`coursecode`, `details`, `intake`, `section`, `facultyname`) VALUES
('CSE 122', 'assignment kore anba', '35', '2', 'arko'),
('CSE 122', 'valo achen', '35', '3', 'arko '),
('CSE 122', 'write a program to count factorial', '35', '2', 'arko'),
('CSE 205', 'fghjnfdnjhd\r\nkikl', '10', '01', 'arko'),
('CSE 205', 'ughjbkjlnhjkn', '10', '01', 'arko'),
('STA 231', 'mnbvcxz', '10', '01', 'arko'),
('CSE 317', 'Hi do it', '30', '02', 'sajib');

-- --------------------------------------------------------

--
-- Table structure for table `classtestdetails`
--

CREATE TABLE `classtestdetails` (
  `coursecode` varchar(50) NOT NULL,
  `details` text NOT NULL,
  `intake` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `facultyname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classtestdetails`
--

INSERT INTO `classtestdetails` (`coursecode`, `details`, `intake`, `section`, `facultyname`) VALUES
('CSE 122', 'hello world print by python', '35', '2', 'arko'),
('CSE 122', 'gffdgfgfg', '30', '02', 'arko'),
('CSE 205', 'ygyjg jhkl knhlkj\r\nhjkl', '10', '01', 'arko');

-- --------------------------------------------------------

--
-- Table structure for table `courseinfo`
--

CREATE TABLE `courseinfo` (
  `username` varchar(50) NOT NULL,
  `coursecode` varchar(50) NOT NULL,
  `coursetitle` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courseinfo`
--

INSERT INTO `courseinfo` (`username`, `coursecode`, `coursetitle`, `status`) VALUES
('ShaaFi', 'CSE 122', 'Object  Oriented Programming Language Lab', '1'),
('ShaaFi', 'CSE 205', 'Digital Logic Design', '0'),
('ShaaFi', 'CSE 317', 'System Analysis and Design', '0'),
('ShaaFi', 'STA 231', 'Statistics', '1'),
('ShaaFi', 'CSE 209', 'Data Communication', '0'),
('ShaaFi', 'CSE 103', 'Discrete Mathematics', '0'),
('ShaaFi', 'MAT 231', 'Complex Variable and Fourier Analysis', '0'),
('ShaaFi', 'CSE 315', 'Microprocessor and Interfacing', '0'),
('ShaaFi', 'ACT 201', 'Accounting Fundamentals', '0'),
('ShaaFi', 'MAT 121', 'Linear Algebras and Differential Equations', '0'),
('ShaaFi', 'CSE 224', 'Numerical Analysis Lab', '0'),
('ShaaFi', 'CSE 207', 'Database Systems', '0'),
('ShaaFi', 'CSE 121', 'Object Oriented Programming Language', '0'),
('ShaaFi', 'CSE 112', 'Structured Programming Language Lab', '0'),
('ShaaFi', 'ENG 111', 'English Language-II', '0'),
('ShaaFi', 'CSE 327', 'Software Engineering', '0'),
('ShaaFi', 'ECO 101', 'Principles of Economics', '0'),
('ShaaFi', 'CSE 331', 'Advanced Programming', '0'),
('ShaaFi', 'CSE 242', 'Algorithms Lab', '0'),
('ShaaFi', 'CSE 215', 'Computer Architecture', '0'),
('ShaaFi', 'EEE 211', 'Electronic Devices and Circuits', '0'),
('ShaaFi', 'MAT 111', 'Co-Ordinate Geometry and Vector Calculus', '0'),
('ShaaFi', 'CSE 241', 'Algorithms', '0'),
('ShaaFi', 'CSE 309', 'Operating Systems', '0'),
('ShaaFi', 'CSE 323', 'Compiler Design', '0'),
('ShaaFi', 'MAT 101', 'Differential and Integral Calculus', '0'),
('ShaaFi', 'CSE 313', 'Mathematical Analysis for Computer Science', '0'),
('ShaaFi', 'EEE 212', 'Electronic Devices and Circuits Lab', '0'),
('ShaaFi', 'CSE 316', 'Microprocessor and Interfacing Lab', '0'),
('ShaaFi', 'CSE 231', 'Data Structures', '0'),
('ShaaFi', 'CSE 111', 'Structured Programming Language', '0'),
('Md. Abdur Rahman', 'CSE 122', 'Object  Oriented Programming Language Lab', '1'),
('Md. Abdur Rahman', 'CSE 205', 'Digital Logic Design', '1'),
('Md. Abdur Rahman', 'CSE 317', 'System Analysis and Design', '1'),
('Md. Abdur Rahman', 'STA 231', 'Statistics', '1'),
('Md. Abdur Rahman', 'CSE 209', 'Data Communication', '1'),
('Md. Abdur Rahman', 'CSE 103', 'Discrete Mathematics', '1'),
('Md. Abdur Rahman', 'MAT 231', 'Complex Variable and Fourier Analysis', '1'),
('Md. Abdur Rahman', 'CSE 315', 'Microprocessor and Interfacing', '1'),
('Md. Abdur Rahman', 'ACT 201', 'Accounting Fundamentals', '1'),
('Md. Abdur Rahman', 'MAT 121', 'Linear Algebras and Differential Equations', '1'),
('Md. Abdur Rahman', 'CSE 224', 'Numerical Analysis Lab', '1'),
('Md. Abdur Rahman', 'CSE 207', 'Database Systems', '1'),
('Md. Abdur Rahman', 'CSE 121', 'Object Oriented Programming Language', '1'),
('Md. Abdur Rahman', 'CSE 112', 'Structured Programming Language Lab', '1'),
('Md. Abdur Rahman', 'ENG 111', 'English Language-II', '1'),
('Md. Abdur Rahman', 'CSE 327', 'Software Engineering', '1'),
('Md. Abdur Rahman', 'ECO 101', 'Principles of Economics', '1'),
('Md. Abdur Rahman', 'CSE 331', 'Advanced Programming', '1'),
('Md. Abdur Rahman', 'CSE 242', 'Algorithms Lab', '1'),
('Md. Abdur Rahman', 'CSE 215', 'Computer Architecture', '1'),
('Md. Abdur Rahman', 'EEE 211', 'Electronic Devices and Circuits', '0'),
('Md. Abdur Rahman', 'MAT 111', 'Co-Ordinate Geometry and Vector Calculus', '0'),
('Md. Abdur Rahman', 'CSE 241', 'Algorithms', '1'),
('Md. Abdur Rahman', 'CSE 309', 'Operating Systems', '0'),
('Md. Abdur Rahman', 'CSE 323', 'Compiler Design', '0'),
('Md. Abdur Rahman', 'MAT 101', 'Differential and Integral Calculus', '0'),
('Md. Abdur Rahman', 'CSE 313', 'Mathematical Analysis for Computer Science', '0'),
('Md. Abdur Rahman', 'EEE 212', 'Electronic Devices and Circuits Lab', '0'),
('Md. Abdur Rahman', 'CSE 316', 'Microprocessor and Interfacing Lab', '0'),
('Md. Abdur Rahman', 'CSE 231', 'Data Structures', '0'),
('Md. Abdur Rahman', 'CSE 111', 'Structured Programming Language', '1'),
('ar', 'CSE 122', 'Object  Oriented Programming Language Lab', '1'),
('ar', 'CSE 205', 'Digital Logic Design', '0'),
('ar', 'CSE 317', 'System Analysis and Design', '0'),
('ar', 'STA 231', 'Statistics', '0'),
('ar', 'CSE 209', 'Data Communication', '0'),
('ar', 'CSE 103', 'Discrete Mathematics', '0'),
('ar', 'MAT 231', 'Complex Variable and Fourier Analysis', '0'),
('ar', 'CSE 315', 'Microprocessor and Interfacing', '0'),
('ar', 'ACT 201', 'Accounting Fundamentals', '0'),
('ar', 'MAT 121', 'Linear Algebras and Differential Equations', '0'),
('ar', 'CSE 224', 'Numerical Analysis Lab', '0'),
('ar', 'CSE 207', 'Database Systems', '0'),
('ar', 'CSE 121', 'Object Oriented Programming Language', '0'),
('ar', 'CSE 112', 'Structured Programming Language Lab', '0'),
('ar', 'ENG 111', 'English Language-II', '0'),
('ar', 'CSE 327', 'Software Engineering', '0'),
('ar', 'ECO 101', 'Principles of Economics', '0'),
('ar', 'CSE 331', 'Advanced Programming', '0'),
('ar', 'CSE 242', 'Algorithms Lab', '0'),
('ar', 'CSE 215', 'Computer Architecture', '0'),
('ar', 'EEE 211', 'Electronic Devices and Circuits', '0'),
('ar', 'MAT 111', 'Co-Ordinate Geometry and Vector Calculus', '0'),
('ar', 'CSE 241', 'Algorithms', '0'),
('ar', 'CSE 309', 'Operating Systems', '0'),
('ar', 'CSE 323', 'Compiler Design', '0'),
('ar', 'MAT 101', 'Differential and Integral Calculus', '0'),
('ar', 'CSE 313', 'Mathematical Analysis for Computer Science', '0'),
('ar', 'EEE 212', 'Electronic Devices and Circuits Lab', '0'),
('ar', 'CSE 316', 'Microprocessor and Interfacing Lab', '0'),
('ar', 'CSE 231', 'Data Structures', '0'),
('ar', 'CSE 111', 'Structured Programming Language', '0'),
('rodro', 'CSE 122', 'Object  Oriented Programming Language Lab', '0'),
('rodro', 'CSE 205', 'Digital Logic Design', '0'),
('rodro', 'CSE 317', 'System Analysis and Design', '0'),
('rodro', 'STA 231', 'Statistics', '0'),
('rodro', 'CSE 209', 'Data Communication', '0'),
('rodro', 'CSE 103', 'Discrete Mathematics', '0'),
('rodro', 'MAT 231', 'Complex Variable and Fourier Analysis', '0'),
('rodro', 'CSE 315', 'Microprocessor and Interfacing', '0'),
('rodro', 'ACT 201', 'Accounting Fundamentals', '0'),
('rodro', 'MAT 121', 'Linear Algebras and Differential Equations', '0'),
('rodro', 'CSE 224', 'Numerical Analysis Lab', '1'),
('rodro', 'CSE 207', 'Database Systems', '0'),
('rodro', 'CSE 121', 'Object Oriented Programming Language', '0'),
('rodro', 'CSE 112', 'Structured Programming Language Lab', '0'),
('rodro', 'ENG 111', 'English Language-II', '0'),
('rodro', 'CSE 327', 'Software Engineering', '0'),
('rodro', 'ECO 101', 'Principles of Economics', '0'),
('rodro', 'CSE 331', 'Advanced Programming', '0'),
('rodro', 'CSE 242', 'Algorithms Lab', '0'),
('rodro', 'CSE 215', 'Computer Architecture', '0'),
('rodro', 'EEE 211', 'Electronic Devices and Circuits', '0'),
('rodro', 'MAT 111', 'Co-Ordinate Geometry and Vector Calculus', '0'),
('rodro', 'CSE 241', 'Algorithms', '0'),
('rodro', 'CSE 309', 'Operating Systems', '1'),
('rodro', 'CSE 323', 'Compiler Design', '0'),
('rodro', 'MAT 101', 'Differential and Integral Calculus', '0'),
('rodro', 'CSE 313', 'Mathematical Analysis for Computer Science', '0'),
('rodro', 'EEE 212', 'Electronic Devices and Circuits Lab', '0'),
('rodro', 'CSE 316', 'Microprocessor and Interfacing Lab', '0'),
('rodro', 'CSE 231', 'Data Structures', '0'),
('rodro', 'CSE 111', 'Structured Programming Language', '0');

-- --------------------------------------------------------

--
-- Table structure for table `courselink`
--

CREATE TABLE `courselink` (
  `coursecode` varchar(50) NOT NULL,
  `link` varchar(1000) NOT NULL,
  `submitlink` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courselink`
--

INSERT INTO `courselink` (`coursecode`, `link`, `submitlink`) VALUES
('ACT 201', 'https://drive.google.com/drive/folders/1yd7mGSHMEt0M_HF7ld45gO1pZ8eIyyIG?usp=sharing', 'https://drive.google.com/drive/folders/1yd7mGSHMEt0M_HF7ld45gO1pZ8eIyyIG?usp=sharing'),
('CSE 103', 'https://drive.google.com/drive/folders/1pMNgbgrlmYz4wZof8idIRBme0i5ziley?usp=sharing', 'https://drive.google.com/drive/folders/1pMNgbgrlmYz4wZof8idIRBme0i5ziley?usp=sharing'),
('CSE 111', 'https://drive.google.com/drive/folders/1PB4HRmqEM_-BJganTf0EEyNZkVRsZ6mI?usp=sharing', 'https://drive.google.com/drive/folders/1PB4HRmqEM_-BJganTf0EEyNZkVRsZ6mI?usp=sharing'),
('CSE 112', 'https://drive.google.com/drive/folders/1semBxvWnPgqbyZCwIds8yNh6wvYj3uE2?usp=sharing', 'https://drive.google.com/drive/folders/1semBxvWnPgqbyZCwIds8yNh6wvYj3uE2?usp=sharing'),
('CSE 121', 'https://drive.google.com/drive/folders/1mFjpjYuE4k9DQWIlo7ZmyksunjXQMDsf?usp=sharing', 'https://drive.google.com/drive/folders/1mFjpjYuE4k9DQWIlo7ZmyksunjXQMDsf?usp=sharing'),
('CSE 122', 'https://drive.google.com/drive/folders/1FWkfuWNoZzc6VkdysJGPjOT4nK5sV83Y?usp=sharing', 'https://drive.google.com/drive/folders/1FWkfuWNoZzc6VkdysJGPjOT4nK5sV83Y?usp=sharing'),
('CSE 205', 'https://drive.google.com/drive/folders/1wFM9ar7xOdOrLT1Z3CEeuXYw-p829t9x?usp=sharing', 'https://drive.google.com/drive/folders/1wFM9ar7xOdOrLT1Z3CEeuXYw-p829t9x?usp=sharing'),
('CSE 207', 'https://drive.google.com/drive/folders/1RUL6_OMcY8BbhZ0m8TfUhUfybq_iu-nY?usp=sharing', 'https://drive.google.com/drive/folders/1RUL6_OMcY8BbhZ0m8TfUhUfybq_iu-nY?usp=sharing'),
('CSE 209', 'https://drive.google.com/drive/folders/1dqxnx3G1EnZUWmc8j86_hhLSJ0eVgek0?usp=sharing', 'https://drive.google.com/drive/folders/1dqxnx3G1EnZUWmc8j86_hhLSJ0eVgek0?usp=sharing'),
('CSE 215', 'https://drive.google.com/drive/folders/1ZIdaaMNYetwILJoNKwkCXr4h2O8Fy-Ny?usp=sharing', 'https://drive.google.com/drive/folders/1ZIdaaMNYetwILJoNKwkCXr4h2O8Fy-Ny?usp=sharing'),
('CSE 224', 'https://drive.google.com/drive/folders/1qYI2H0pdU7H7RpmKhQ0nwmSKiarfcqCS?usp=sharing', 'https://drive.google.com/drive/folders/1qYI2H0pdU7H7RpmKhQ0nwmSKiarfcqCS?usp=sharing'),
('CSE 231', 'https://drive.google.com/drive/folders/127lnT_o3mcBF-me1MuPfZ9Py49wgPISX?usp=sharing', 'https://drive.google.com/drive/folders/127lnT_o3mcBF-me1MuPfZ9Py49wgPISX?usp=sharing'),
('CSE 241', 'https://drive.google.com/drive/folders/1gQNHPgu_v9Cft9IMe9oED6OWc7ZNCHBO?usp=sharing', 'https://drive.google.com/drive/folders/1gQNHPgu_v9Cft9IMe9oED6OWc7ZNCHBO?usp=sharing'),
('CSE 242', 'https://drive.google.com/drive/folders/1qBwTRRZKGnEu2FnHiBQjLz_b7TEaXCwE?usp=sharing', 'https://drive.google.com/drive/folders/1qBwTRRZKGnEu2FnHiBQjLz_b7TEaXCwE?usp=sharing'),
('CSE 309', 'https://drive.google.com/drive/folders/1J-gtiYX3FUswgmC73Yp3Be9ol1zkGQM5?usp=sharing', 'https://drive.google.com/drive/folders/1J-gtiYX3FUswgmC73Yp3Be9ol1zkGQM5?usp=sharing'),
('CSE 313', 'https://drive.google.com/drive/folders/1YS28DNbuGzgUFGYC8uQEbeHCowqSGAxC?usp=sharing', 'https://drive.google.com/drive/folders/1YS28DNbuGzgUFGYC8uQEbeHCowqSGAxC?usp=sharing'),
('CSE 315', 'https://drive.google.com/drive/folders/1t88EnrgYleG1ly3Kyo8BsXXCU0cPhQJc?usp=sharing', 'https://drive.google.com/drive/folders/1t88EnrgYleG1ly3Kyo8BsXXCU0cPhQJc?usp=sharing'),
('CSE 316', 'https://drive.google.com/drive/folders/1UYVloTzqWRGJ-u30Hu0DBi-YATOaroqk?usp=sharing', 'https://drive.google.com/drive/folders/1UYVloTzqWRGJ-u30Hu0DBi-YATOaroqk?usp=sharing'),
('CSE 317', 'https://drive.google.com/drive/folders/1ZY4WFLUYu1T9o29UNIeCxK3fXwKZjFuu?usp=sharing', 'https://drive.google.com/drive/folders/1ZY4WFLUYu1T9o29UNIeCxK3fXwKZjFuu?usp=sharing'),
('CSE 323', 'https://drive.google.com/drive/folders/1foCygGN9GAwVWTKfcvlqAgGRzTMwC238?usp=sharing', 'https://drive.google.com/drive/folders/1foCygGN9GAwVWTKfcvlqAgGRzTMwC238?usp=sharing'),
('CSE 327', 'https://drive.google.com/drive/folders/1yZLO5Cnwy8VZVXJcqqcnVMzeABwfwnPX?usp=sharing', 'https://drive.google.com/drive/folders/1yZLO5Cnwy8VZVXJcqqcnVMzeABwfwnPX?usp=sharing'),
('CSE 331', 'https://drive.google.com/drive/folders/10mE2Zj330f8V35xsNan-hRFbON9yi3MH?usp=sharing', 'https://drive.google.com/drive/folders/10mE2Zj330f8V35xsNan-hRFbON9yi3MH?usp=sharing'),
('ECO 101', 'https://drive.google.com/drive/folders/1yBBwyW9h2EvxPfqPl8GQUQLfHnKptU_I?usp=sharing', 'https://drive.google.com/drive/folders/1yBBwyW9h2EvxPfqPl8GQUQLfHnKptU_I?usp=sharing'),
('EEE 211', 'https://drive.google.com/drive/folders/15dAaPAdn1C0RJQ-evBntSY0TStD65h1W?usp=sharing', 'https://drive.google.com/drive/folders/15dAaPAdn1C0RJQ-evBntSY0TStD65h1W?usp=sharing'),
('EEE 212', 'https://drive.google.com/drive/folders/1x7KVQomH73Je4UkUP3PIeuONosG_umEE?usp=sharing', 'https://drive.google.com/drive/folders/1x7KVQomH73Je4UkUP3PIeuONosG_umEE?usp=sharing'),
('ENG 111', 'https://drive.google.com/drive/folders/1sXZor-qmx1-3E3p98yWbnuBEqXSXY4Jx?usp=sharing', 'https://drive.google.com/drive/folders/1sXZor-qmx1-3E3p98yWbnuBEqXSXY4Jx?usp=sharing'),
('MAT 101', 'https://drive.google.com/drive/folders/1UauYIt4cBubXuV1nIAK2rvOFXKE2SuIt?usp=sharing', 'https://drive.google.com/drive/folders/1UauYIt4cBubXuV1nIAK2rvOFXKE2SuIt?usp=sharing'),
('MAT 111', 'https://drive.google.com/drive/folders/1um5d6ZoZpVo008xsvZ_NPYCo54hZ2rX7?usp=sharing', 'https://drive.google.com/drive/folders/1um5d6ZoZpVo008xsvZ_NPYCo54hZ2rX7?usp=sharing'),
('MAT 121', 'https://drive.google.com/drive/folders/1WLes2toI-5ei-_qm5rWnFxyOdx-P1-al?usp=sharing', 'https://drive.google.com/drive/folders/1WLes2toI-5ei-_qm5rWnFxyOdx-P1-al?usp=sharing'),
('MAT 231', 'https://drive.google.com/drive/folders/13nrK500YAtP-7BRBDxrA8MM_j9PwADXB?usp=sharing', 'https://drive.google.com/open?id=13nrK500YAtP-7BRBDxrA8MM_j9PwADXB'),
('STA 231', 'https://drive.google.com/drive/folders/1XfRkWxs-imyWoJ2gugh4V8dihwzWgr3u?usp=sharing', 'https://drive.google.com/drive/folders/13nrK500YAtP-7BRBDxrA8MM_j9PwADXB?usp=sharing');

-- --------------------------------------------------------

--
-- Table structure for table `facultyinfo`
--

CREATE TABLE `facultyinfo` (
  `facultyname` varchar(50) NOT NULL,
  `fpassword` varchar(50) NOT NULL,
  `femail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facultyinfo`
--

INSERT INTO `facultyinfo` (`facultyname`, `fpassword`, `femail`) VALUES
('arko', 'admin', 'arko@gmail.com'),
('sajib', 'admin', 'sajib@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `fcourseinfo`
--

CREATE TABLE `fcourseinfo` (
  `coursecode` varchar(50) NOT NULL,
  `coursetitle` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `facultyname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcourseinfo`
--

INSERT INTO `fcourseinfo` (`coursecode`, `coursetitle`, `status`, `facultyname`) VALUES
('CSE 122', 'Object  Oriented Programming Language Lab', '1', 'arko'),
('CSE 205', 'Digital Logic Design', '1', 'arko'),
('CSE 317', 'System Analysis and Design', '1', 'arko'),
('STA 231', 'Statistics', '1', 'arko'),
('CSE 209', 'Data Communication', '1', 'arko'),
('CSE 103', 'Discrete Mathematics', '1', 'arko'),
('MAT 231', 'Complex Variable and Fourier Analysis', '0', 'arko'),
('CSE 315', 'Microprocessor and Interfacing', '0', 'arko'),
('ACT 201', 'Accounting Fundamentals', '0', 'arko'),
('MAT 121', 'Linear Algebras and Differential Equations', '0', 'arko'),
('CSE 224', 'Numerical Analysis Lab', '0', 'arko'),
('CSE 207', 'Database Systems', '0', 'arko'),
('CSE 121', 'Object Oriented Programming Language', '0', 'arko'),
('CSE 112', 'Structured Programming Language Lab', '0', 'arko'),
('ENG 111', 'English Language-II', '0', 'arko'),
('CSE 327', 'Software Engineering', '0', 'arko'),
('ECO 101', 'Principles of Economics', '0', 'arko'),
('CSE 331', 'Advanced Programming', '0', 'arko'),
('CSE 242', 'Algorithms Lab', '0', 'arko'),
('CSE 215', 'Computer Architecture', '0', 'arko'),
('EEE 211', 'Electronic Devices and Circuits', '0', 'arko'),
('MAT 111', 'Co-Ordinate Geometry and Vector Calculus', '0', 'arko'),
('CSE 241', 'Algorithms', '0', 'arko'),
('CSE 309', 'Operating Systems', '0', 'arko'),
('CSE 323', 'Compiler Design', '0', 'arko'),
('MAT 101', 'Differential and Integral Calculus', '0', 'arko'),
('CSE 313', 'Mathematical Analysis for Computer Science', '0', 'arko'),
('EEE 212', 'Electronic Devices and Circuits Lab', '0', 'arko'),
('CSE 316', 'Microprocessor and Interfacing Lab', '0', 'arko'),
('CSE 231', 'Data Structures', '0', 'arko'),
('CSE 111', 'Structured Programming Language', '1', 'arko'),
('CSE 122', 'Object  Oriented Programming Language Lab', '0', 'sajib'),
('CSE 205', 'Digital Logic Design', '0', 'sajib'),
('CSE 317', 'System Analysis and Design', '1', 'sajib'),
('STA 231', 'Statistics', '0', 'sajib'),
('CSE 209', 'Data Communication', '0', 'sajib'),
('CSE 103', 'Discrete Mathematics', '0', 'sajib'),
('MAT 231', 'Complex Variable and Fourier Analysis', '0', 'sajib'),
('CSE 315', 'Microprocessor and Interfacing', '1', 'sajib'),
('ACT 201', 'Accounting Fundamentals', '0', 'sajib'),
('MAT 121', 'Linear Algebras and Differential Equations', '0', 'sajib'),
('CSE 224', 'Numerical Analysis Lab', '0', 'sajib'),
('CSE 207', 'Database Systems', '0', 'sajib'),
('CSE 121', 'Object Oriented Programming Language', '0', 'sajib'),
('CSE 112', 'Structured Programming Language Lab', '0', 'sajib'),
('ENG 111', 'English Language-II', '0', 'sajib'),
('CSE 327', 'Software Engineering', '0', 'sajib'),
('ECO 101', 'Principles of Economics', '0', 'sajib'),
('CSE 331', 'Advanced Programming', '0', 'sajib'),
('CSE 242', 'Algorithms Lab', '0', 'sajib'),
('CSE 215', 'Computer Architecture', '0', 'sajib'),
('EEE 211', 'Electronic Devices and Circuits', '0', 'sajib'),
('MAT 111', 'Co-Ordinate Geometry and Vector Calculus', '0', 'sajib'),
('CSE 241', 'Algorithms', '0', 'sajib'),
('CSE 309', 'Operating Systems', '0', 'sajib'),
('CSE 323', 'Compiler Design', '0', 'sajib'),
('MAT 101', 'Differential and Integral Calculus', '0', 'sajib'),
('CSE 313', 'Mathematical Analysis for Computer Science', '0', 'sajib'),
('EEE 212', 'Electronic Devices and Circuits Lab', '0', 'sajib'),
('CSE 316', 'Microprocessor and Interfacing Lab', '0', 'sajib'),
('CSE 231', 'Data Structures', '0', 'sajib'),
('CSE 111', 'Structured Programming Language', '0', 'sajib');

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE `information` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `intake` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `information`
--

INSERT INTO `information` (`username`, `password`, `email`, `intake`, `section`) VALUES
('ar', '123456', 'ar@gmail.com', '10', '01'),
('Md. Abdur Rahman', 'admin', 'arcse3048@gmail.com', '30', '02'),
('rodro', 'rodro', 'rodro@gmail.com', '30', '02'),
('ShaaFi', 'admin', 'shaafi.fahad@gmail.com', '35', '2');

-- --------------------------------------------------------

--
-- Table structure for table `submitassignment`
--

CREATE TABLE `submitassignment` (
  `facultyname` varchar(50) NOT NULL,
  `coursecode` varchar(50) NOT NULL,
  `studentname` varchar(50) NOT NULL,
  `sumbission` longtext NOT NULL,
  `intake` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `image` blob NOT NULL,
  `imagename` varchar(50) NOT NULL,
  `listno` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submitassignment`
--

INSERT INTO `submitassignment` (`facultyname`, `coursecode`, `studentname`, `sumbission`, `intake`, `section`, `image`, `imagename`, `listno`) VALUES
('arko ', 'CSE 122 ', 'ShaaFi   ', 'none', '35 ', '2', 0x637573746f6d65722070617373776f7264206368616e67652e706e67, 'oop1', 9),
('arko ', 'CSE 122 ', 'ShaaFi  ', 'none', '35 ', '2', '', '', 10),
('arko', 'CSE 205', 'ar ', 'solved', '10 ', '01', '', 'no image', 12),
('sajib', 'CSE 317', 'Md. Abdur Rahman ', 'Done!!!\r\nTnk u', '30 ', '02', '', 'no image', 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courselink`
--
ALTER TABLE `courselink`
  ADD PRIMARY KEY (`coursecode`);

--
-- Indexes for table `facultyinfo`
--
ALTER TABLE `facultyinfo`
  ADD PRIMARY KEY (`facultyname`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `submitassignment`
--
ALTER TABLE `submitassignment`
  ADD PRIMARY KEY (`listno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `submitassignment`
--
ALTER TABLE `submitassignment`
  MODIFY `listno` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
